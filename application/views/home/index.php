<div class="fix-width" style="padding: 0 !important; background-repeat: no-repeat; background-size: cover; background-position: bottom; margin-top: -10px !important;">


    <div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators3" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators3" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators3" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators3" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <img class="img-responsive" src="<?php echo base_url('themes/landingpage/') ?>assets/images/big/1.bak.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="img-responsive" src="<?php echo base_url('themes/landingpage/') ?>assets/images/big/4.bak.jpg" alt="Second slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<div class="row white-space">
    <div class="col-md-12 ">
        <div class="fix-width icon-section">
            <div class="text-center m-b-20">
                <h2 class="display-7"><b>5 PRIORITAS CSR KOTA CIREBON</b></h2>
                <!-- <h3 class="display-9">Beberapa Program Unggulan kami yang telah berhasil memberikan manfaat bagi masyarakat</h3> -->
            </div>
            <!-- Row -->
            <div class="row m-t-40">
                <!-- .col -->
                <div class="col-lg-6 col-md-12"> <i class="ti-paint-bucket display-5 text-info"></i>
                    <h4 class="font-500">BIDANG INFRASTRUKTUR</h4>
                    <p>Penataan infrastruktur wilayah, mencakup : </p>
                    <ul>
                        <li>Penyediaan dan penataan Ruang Terbuka Hijau (RTH) </li>
                        <li>Revitalisasi sarana umum </li>
                        <li>Revitalisasi bangunan-bangunan bersejarah </li>
                        <li>Penyediaan sumber energi ramah lingkungan </li>
                        <li>Pembangunan dan rehabilitasi jalan lingkungan </li>
                        <li>Pembangunan dan rehabilitasi saluran drainase dan/atau </li>
                        <li>Pembangunan dan rehabilitasi pedestrian.</li>
                    </ul>
                    <p>Lingkungan hidup : </p>
                    <ul>
                        <li>Penerapan sistem pengelolaan reaktor terpadu berbasis Rumah tangga; </li>
                        <li>Bimbingan teknis usaha pengelolaan sampah terpadu;  </li>
                        <li>Fasilitasi gerakan masyarakat yang terkait dengan pelestarian lingkungan; dan/atau </li>
                        <li>Bantuan peralatan untuk pengelolaan sampah; </li>
                        <li>Bantuan sarana dan prasarana penangulangan bencana/perubahan iklim. </li>
                    </ul>
                </div>
                <br>
                <!-- /.col -->
                <!-- .col -->
                <div class="col-lg-6 col-md-6"><i class="ti-layers-alt display-5 text-info"></i>
                    <h4 class="font-500">BIDANG KESEHATAN</h4>
                    <p>Peningkatan mutu SDM bidang kesehatan dengan pelatihan-pelatihan yang mampu mengakselerasi peningkatan pembangunan bidang kesehatan.</p>
                    <p>Peningkatan perilaku hidup sehat:</p>
                    <ul>
                        <li>gerakan ibu terampil dan warga sehat</li>
                        <li>Forum kelurahan sehat</li>
                        <li>donor darah; dan/atau </li>
                        <li>sosialisasi penanggulangan penyakit menular (HIV, NAPZA dan lain-lain).</li>
                    </ul>
                    <p>Peningkatan sarana dan prasarana pelayanan kesehatan dan sanitasi lingkungan:</p>
                    <ul>
                        <li>penyediaan air bersih melalui hydrant umum</li>
                        <li>pembangunan septic tank komunal</li>
                        <li>fasilitas air bersih; dan/atau </li>
                        <li>fasilitas pelayanan kesehatan.</li>
                    </ul>
                    <p>Pelayanan kesehatan massal.</p>
                </div>
                <!-- /.col -->
                <br>
                <!-- .col -->
                <div class="col-lg-6 col-md-6"><i class="ti-paint-roller display-5 text-info"></i>
                    <h4 class="font-500">BIDANG SOSIAL BUDAYA</h4>
                    <p>penanganan dan pemenuhan hak anak-anak terlantar, mencakup : </p>
                    <ul>
                        <li>pembangunan dan pemeliharaan rumah singgah; </li>
                        <li>pemberian edukasi atau pendidikan; </li>
                        <li>layanan kesehatan;  </li>
                        <li>penyediaan sarana dan prasarana perpustakaan;</li>
                        <li>penyediaan bahan bacaan untuk perpustakaan sekolah atau umum; dan/atau  </li>
                        <li>pembangunan dan pemeliharaan bangunan/ gedung perpustakaan/rumah baca. </li>
                    </ul>
                    <p>Perlindungan seni dan budaya tradisional dalam masyarakat :</p>
                    <ul>
                        <li>Pendaftaran Hak atas Kekayaan Intelektual (HAKI) seni dan budaya tradisional; dan</li>
                        <li>Pameran atau gelar seni budaya.  </li>
                    </ul>
                    <p>pembangunan sarana seni dan budaya : </p>
                    <ul>
                        <li>revitalisasi sarana dan prasarana seni dan budaya; dan </li>
                        <li>penguatan kearifan lokal  </li>
                    </ul>
                    <p>bantuan pembangunan dan pemeliharaan sarana prasarana peribadatan; </p>
                    <p>bantuan peringatan hari–hari besar Nasional; dan</p>
                    <p>bantuan lomba dan kegiatan sosial masyarakat lainnya. </p>
                </div>
                <br>
                <div class="col-lg-6 col-md-6"><i class="ti-paint-roller display-5 text-info"></i>
                    <h4 class="font-500">BIDANG EKONOMI</h4>
                    <p>Kewirausahaan dan kemandirian, mencakup:</p>
                    <ul>
                        <li>Diklat kewirausahaan; </li>
                        <li>Bimbingan teknis kewirausahaan spesifik wilayah; </li>
                        <li>Magang ketenagakerjaan usia produktif pada berbagai usaha  </li>
                    </ul>
                    <p>Pengembangan produk UMKM, mencakup: </p>
                    <ul>
                        <li>Pembentukan Kelompok Usaha Bersama (KUBE) dan lembaga keuangan mikro tingkat Kecamatan/Kelurahan;  </li>
                        <li>Bimbingan teknis usaha kecil berbasis lokal; </li>
                        <li>Pendampingan UMKM; </li>
                        <li>Promosi usaha bekerjasama dengan media massa</li>
                        <li>Bimbingan pemasaran produk UMKM; dan/atau </li>
                        <li>Pameran produk unggulan UMKM. </li>
                    </ul>
                </div>
                <br>
                <!-- /.col -->
                <div class="col-lg-12 col-md-6"> <i class="ti-layout-sidebar-right display-5 text-info"></i>
                    <h4 class="font-500">BIDANG PENDIDIKAN</h4>
                    <p>Pembangunan dan pengembangan sarana dan prasarana, pendidikan masyarakat, mencakup : </p>
                    <ul>
                        <li>rehabilitasi ruang kelas Sekolah Dasar, Sekolah Menengah Pertama, dan Sekolah Menengah Atas.</li>
                        <li>pembangunan laboratorium lapangan berorientasi pendidikan masyarakat (lifeskill education); </li>
                        <li>pengembangan perpustakaan kelurahan dan kecamatan sebagai bagian sarana belajar masyarakat; dan/atau  </li>
                        <li>pengadaan buku-buku untuk perpustakaan sekolah dan perpustakaan sarana belajar masyarakat. </li>
                    </ul>
                    <p>Pendampingan dan penyuluhan pendidikan luar sekolah bermuatan motivasi berprestasi dan budaya : </p>
                    <ul>
                        <li>Pendampingan masyarakat terutama berkaitan dengan motivasi dan prestasi; dan/atau  </li>
                        <li>Penyuluhan tentang bahaya merokok dan narkoba serta etika budaya bangsa. </li>
                    </ul>
                    <p>Program bimbingan kreativitas anak; </p>
                    <ul>
                        <li>Lomba kreasi anak; </li>
                        <li>Beasiswa anak-anak berprestasi; dan/atau </li>
                        <li>Beasiswa anak asuh  </li>
                    </ul>
                    <p>Program bimbingan kreativitas anak; </p>
                    <ul>
                        <li>Lomba kreasi anak; </li>
                        <li>Beasiswa anak-anak berprestasi; dan/atau </li>
                        <li>Beasiswa anak asuh  </li>
                    </ul>
                    <p>Penguatan sarana olahraga, mencakup: </p>
                    <ul>
                        <li>Pembangunan gedung olahraga; </li>
                        <li>Penyediaan alat-alat olahraga bagi siswa sekolah; dan/atau </li>
                        <li>Pengembangan pusat informasi pendidikan dan pelatihan keolahragaan. </li>
                    </ul>
                    <p>Kaderisasi atlet olahraga berprestasi, mencakup:  </p>
                    <ul>
                        <li>sosialisasi cabang olahraga pada anak-anak; dan/atau </li>
                        <li>pekan olahraga antar Sekolah.</li>
                    </ul>
                    <p>Pendukungan atlet berprestasi dengan menjadi sponsorship pada event olahraga. </p>
                </div>
                <br>
            </div>
        </div>
        <br/><br/>
        <br/><br/>
    </div>
    <!-- peta -->
    <div class="col-md-12">
        <div class="text-center m-b-20">
            <h2 class="display-7"><b>Peta Realisasi dan Permohonan Proram TJSL</b></h2>
        </div>
        <br>
        <center>
            <div class="col-md-6">
                <select class="select2 form-control custom-select" style="width: 300px; height:36px;">
                    <option>Pilih Jenis Permohonan</option>
                        <option value="AK">Musrenbang</option>
                        <option value="HI">Proposal</option>
                        <option value="HI">Realisasai</option>
                </select>
            </div>
        </center> <br><br>
        <div class="container terasi">
            <div class="col-md-12">
                <div id="markermap" class="gmaps"></div>
            <div>
        <div>
        <br/><br/>
        <br/><br/>
    </div>
    <!-- ////// -->
    <!-- alur partisipasi pembangunan -->
    <div class="container terasi">
        <div class="col-md-12">
            <div class="text-center m-b-20">
                <h2 class="display-7"><b>Alur Partisipasi Program Pembangunan</b></h2>
            </div>
            <div class="card">
                <div class="col-md-12">
                    <a class="image-popup-no-margins" href="<?php echo base_url('themes/landingpage/') ?>assets/images/big/alur.jpg"> <img src="<?php echo base_url('themes/landingpage/') ?>assets/images/big/alur.jpeg" alt="image" class="img-responsive" style="height: 1000px"/> </a>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="col-md-12">
                    <a class="image-popup-no-margins" href="<?php echo base_url('themes/landingpage/') ?>assets/images/big/mekanisme.jpeg"> <img src="<?php echo base_url('themes/landingpage/') ?>assets/images/big/mekanisme.jpeg" alt="image" class="img-responsive" style="height: 1000px"/> </a>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="col-md-12">
                    <a class="image-popup-no-margins" href="<?php echo base_url('themes/landingpage/') ?>assets/images/big/hibah.jpeg"> <img src="<?php echo base_url('themes/landingpage/') ?>assets/images/big/hibah.jpeg" alt="image" class="img-responsive" style="height: 1000px"/> </a>
                </div>
            </div>
            <br><br><br>
        </div>
        <br/><br/><br/><br/>
    </div>
    <!-- ///// -->

    <!-- Logo  -->
</div>
</div>
</div>
</div>
</div>
<a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>