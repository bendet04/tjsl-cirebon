<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notifikasi_model extends BF_Model {

	public function __construct()
	{
		parent::__construct();
	}

    public function insert_notifikasi($id_user, $pesan, $id_tujuan){
        $data = array('id_user'=>$id_user,
						'pesan'=>$pesan,
						'id_tujuan'=> $id_tujuan,
						'created_at'=> date('Y-m-d h:i:s'));
        $this->db->insert('notifikasi', $data);
    }

    public function get_notifikasi($id_tujuan){
        $data =$this->db->select('*')
                    ->from('notifikasi')
                    ->where('id_tujuan', $id_tujuan)
                    ->where('status', '0')
                    ->get()->result();

        return $data;
    }

	public function delet_notif($id, $pesan){
		$this->db->where('id_tujuan', $id);
		$this->db->where('pesan', $pesan);
		$this->db->delete('notifikasi`');
    }


}
