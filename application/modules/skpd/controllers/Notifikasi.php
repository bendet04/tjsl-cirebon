<?php defined('BASEPATH') || exit('No direct script access allowed');


class Notifikasi extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('notifikasi_model');
    }

    public function insert_notifikasi($id_user, $pesan, $id_tujuan){
        $data = $this->notifikasi_model->insert_notifikasi($id_user, $pesan, $id_tujuan);
    }

    public function get_notifikasi(){
        $id_tujuan = $this->input->post('id_tujuan');
        $data = $this->notifikasi_model->get_notifikasi($id_tujuan);
        $notif = '';
        foreach ($data as $key) {
            if ($key->pesan == 1){
                $title = 'Pengjuan Verifikasi Perusahaan';
                $url = site_url('/skpd/perusahaan');
                $icon ='<div class="btn btn-danger btn-circle"><i class="fa fa-link"></i>';
            } elseif($key->pesan == 2){
                $title = 'Permohonan perusahaan telah di acc';
                $url = site_url('/office/dashboard');
                $icon ='<div class="btn btn-danger btn-circle"><i class="fa fa-link"></i>';
            } elseif($key->pesan == 3){
                $title = 'Pengjuan Permohonan TJSL';
                $url = site_url('/skpd/permohonan_tjsl');
                $icon ='<div class="btn btn-danger btn-circle"><i class="fa fa-link"></i>';
            } elseif($key->pesan == 4){
                $title = 'Permohonan TJSL';
                $url = site_url('/office/permohonan_tjsl');
                $icon ='<div class="btn btn-danger btn-circle"><i class="fa fa-link"></i>';
            } elseif($key->pesan == 5){
                $title = 'Permohonan TJSL telah di reallisasi';
                $url = site_url('/skpd/realisasi_tjsl');
                $icon ='<div class="btn btn-danger btn-circle"><i class="fa fa-link"></i>';
            } elseif($key->pesan == 6){
                $pesan = '';
                $url = '';
            }
            $notif = $notif.'<a href="'.$url.'">'.$icon.'</div><div class="mail-contnet"><h5> '.$title.'</h5> <span class="time"> '.$key->created_at.'</span> </div></a>';

        }
        echo $notif;
    }

    public function delet_notif(){
        $id_tujuan = $this->input->post('id_tujuan');
        $pesan = $this->input->post('pesan');
         $this->notifikasi_model->delet_notif($id_tujuan, $pesan);
         echo ($id_tujuan.' '.$pesan);
    }
}
