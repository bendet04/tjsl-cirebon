 <script type="text/javascript">
   var url = "<?php echo site_url('skpd/dashboard/') ?>";
</script>
 <div class="card-group">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h2 class="m-b-0"><i class="mdi mdi-briefcase-check text-info"></i></h2>
                    <h3 class=""> <span  id="jumlah_permohonan_tjsl">0 </span></h3>
                    <h6 class="card-subtitle">Permohonan Proposal TJSL</h6></div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h2 class="m-b-0"><i class="mdi mdi-alert-circle text-success"></i></h2>
                    <h3 class=""><span  id="jumlah_permohonan_tjsl_acc">0</span></h3>
                    <h6 class="card-subtitle">ACC Permohonan TJSL</h6></div>

            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h2 class="m-b-0"><i class="mdi mdi-wallet text-purple"></i></h2>
                    <h3 class=""><span  id="jumlah_permohonan_tjsl_realisasi">0</span></h3>
                    <h6 class="card-subtitle">Realisasi Permohonan TJSL</h6></div>

            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h2 class="m-b-0"><i class="mdi mdi-buffer text-warning"></i></h2>
                    <h3 class="">Rp. <span  id="total_realisasi">0</span></h3>
                    <h6 class="card-subtitle">Total Realisasi TJSL</h6></div>

            </div>
        </div>
    </div>
</div>
         <!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">


    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data Realisasi TJSL Pemerintah Kota Cirebon</h4>
                <div id="realisasi"></div>
            </div>
        </div>
    </div>
    <!-- column -->
</div>
 <div class="row">


    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Daftar Realisasi Jenis Perusahaan </h4>
                <div id="realisasi_tipe_perusahaan"></div>
            </div>
        </div>
    </div>
    <!-- column -->
</div>
