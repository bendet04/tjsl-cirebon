<script type="text/javascript">
var url = "<?php echo site_url('skpd/perusahaan/') ?>";
</script>
<?php

$errorClass   = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$fieldData = array(
    'errorClass'    => $errorClass,
    'controlClass'  => $controlClass,
    );

    ?>
    <style scoped='scoped'>
        #register p.already-registered {
            text-align: center;
        }
    </style>
    <section id="wrapper">
        <div class="row" style="background-color: #FFF">
            <div class="login-box card">
                <div class="card-body">
                   <center> <h2 class="page-header"><?php echo lang('us_sign_up'); ?></h2></center>
                   <br>
   <!--  <?php if (validation_errors()) : ?>
    <div class="alert alert-error fade in">
        <?php echo validation_errors(); ?>
    </div>
    <?php endif; ?>

        <?php
        if (isset($password_hints)) {
            echo $password_hints;
        }
        ?>
    </div> -->

    <?php echo form_open(site_url(REGISTER_URL), array('class' => "form-horizontal", 'autocomplete' => 'off','enctype'=>'multipart/form-data')); ?>
    <fieldset>
        <?php Template::block('user_fields', 'user_fields', $fieldData); ?>
    </fieldset>
    <fieldset>
        <?php
                    // Allow modules to render custom fields. No payload is passed
                    // since the user has not been created, yet.
        Events::trigger('render_user_form');
        ?>
        <!-- Start of User Meta -->
        <!-- <?php $this->load->view('users/user_meta', array('frontend_only' => false)); ?>  -->
        <!-- End of User Meta -->
    </fieldset>
    <fieldset>

        <div class="form-group text-center m-t-20">
            <div class="col-md-12">
                <input class="btn btn-primary" type="submit" name="register" id="submit" value="<?php echo lang('us_register'); ?>" />

            </div>
        </div>
    </fieldset>
    <div class="form-group m-b-0">
        <div class="col-sm-12 text-center">
            <?php echo form_close(); ?>

        </div>
    </div>

</div>
</div>
</div>
</section>
