
<?php if(!empty($status_perusahaan)){?>
    <?php foreach($status_perusahaan as $row){?>
        <?php if($row->status_validasi == 0){?>

            <li class="nav-devider"></li>
            <li class="nav-small-cap">GENERAL</li>
            <li style="pointer-event:none; opacity:0.6"> <a class=" waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>

            </li>
            <li class="nav-small-cap">MANAJEMEN PROGRAM TJSL</li>
            <li style="pointer-event:none; opacity:0.6"> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Proposal TJSL</span></a>
                <ul aria-expanded="false" class="collapse">
                    <li><a href="#">Permohonan Proposal Tjsl</a></li>
                    <li><a href="#">Proses Realisasi TJSL</a></li>
                </ul>
            </li>
            <li style="pointer-event:none; opacity:0.6"> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Realisasi Program TJSL</span></a>
                <ul aria-expanded="false" class="collapse">
                    <li style="pointer-event:none; opacity:0.6"><a href="#">List Realisasi TJSL</a></li>
                </ul>
            </li>
            <li class="nav-devider"></li>
            <li><a href="<?php echo site_url('logout'); ?>"><i class="mdi mdi-book-multiple"></i>Logout</a></li>
        <?php } else{ ?>
            <li class="nav-devider"></li>
            <li class="nav-small-cap">GENERAL</li>
            <li> <a class=" waves-effect waves-dark" href="<?php echo site_url('office/dashboard') ?>" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>

            </li>
            <li class="nav-small-cap">MANAJEMEN PROGRAM TJSL</li>
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Proposal TJSL</span></a>
                <ul aria-expanded="false" class="collapse">
                    <li><a href="<?php echo site_url('office/permohonan_tjsl') ?>">Permohonan Proposal Tjsl</a></li>
                    <li><a href="<?php echo site_url('office/proses_realisasi_tjsl') ?>">Proses Realisasi TJSL</a></li>
                </ul>
            </li>
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Realisasi Program TJSL</span></a>
                <ul aria-expanded="false" class="collapse">
                    <li style="pointer-event:none; opacity:0.6"><a href="<?php echo site_url('office/realisasi_tjsl') ?>">List Realisasi TJSL</a></li>
                </ul>
            </li>
            <li class="nav-devider"></li>
            <li><a href="<?php echo site_url('logout'); ?>"><i class="mdi mdi-book-multiple"></i>Logout</a></li>
    <?php } ?>
    <?php } ?>
<?php } else{ ?>
    <li class="nav-devider"></li>
    <li class="nav-small-cap">GENERAL</li>
    <li> <a class=" waves-effect waves-dark" href="<?php echo site_url('office/dashboard') ?>" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>

    </li>
    <li class="nav-small-cap">MANAJEMEN PROGRAM TJSL</li>
    <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Proposal TJSL</span></a>
        <ul aria-expanded="false" class="collapse">
            <li><a href="<?php echo site_url('office/permohonan_tjsl') ?>">Permohonan Proposal Tjsl</a></li>
            <li><a href="<?php echo site_url('office/proses_realisasi_tjsl') ?>">Proses Realisasi TJSL</a></li>
        </ul>
    </li>
    <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Realisasi Program TJSL</span></a>
        <ul aria-expanded="false" class="collapse">
            <li style="pointer-event:none; opacity:0.6"><a href="<?php echo site_url('office/realisasi_tjsl') ?>">List Realisasi TJSL</a></li>
        </ul>
    </li>
    <li class="nav-devider"></li>
    <li><a href="<?php echo site_url('logout'); ?>"><i class="mdi mdi-book-multiple"></i>Logout</a></li>
<?php } ?>
